create table users
(
	id uuid not null
		constraint user_pk
			primary key,
	auth_id text not null,
	creation_date date default now() not null
);

comment on table users is 'база пользователей';

create table lists
(
	id uuid not null
		constraint table_name_pk
			primary key,
	name text not null,
	is_archive boolean,
	user_id uuid not null
		constraint fk_user_id
			references users,
	date_create date default now() not null,
	date_update date default now()
);

comment on table lists is 'база todo листов';

create table lists_elem
(
	id uuid,
	lists_id uuid
		constraint fk_list_id
			references lists,
	description text not null,
	done boolean
);

comment on table lists_elem is 'элементы списка';

create table es_queue
(
	entity_id uuid not null,
	date_create date default now()
);

comment on table es_queue is 'очередь недошедших событий';

create unique index es_queue_entity_id_uindex
	on es_queue (entity_id);

