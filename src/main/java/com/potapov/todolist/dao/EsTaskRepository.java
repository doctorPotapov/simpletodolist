package com.potapov.todolist.dao;

import com.potapov.todolist.model.EsTask;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface EsTaskRepository extends CrudRepository<EsTask, UUID> {
}
