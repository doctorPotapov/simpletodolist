package com.potapov.todolist.dao;

import com.potapov.todolist.configurations.settings.ElasticSearchSettings;
import com.potapov.todolist.dto.ResponseSearchDto;
import com.potapov.todolist.dto.todolist.ToDoListDto;
import com.potapov.todolist.util.JsonOperationUtil;
import lombok.RequiredArgsConstructor;
import org.elasticsearch.action.DocWriteRequest;
import org.elasticsearch.action.admin.indices.template.get.GetIndexTemplatesRequest;
import org.elasticsearch.action.admin.indices.template.get.GetIndexTemplatesResponse;
import org.elasticsearch.action.admin.indices.template.put.PutIndexTemplateRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

@Repository
@RequiredArgsConstructor
public class EsReposotory {

    private final ElasticSearchSettings elasticSearchSettings;

    private final RestHighLevelClient client;

    @Value("${elasticsearch.indexJournalName}")
    private String journalIndexName;

    @Value("${elasticsearch.typeJournalName}")
    private String journalTypeName;

    @Value("${elasticsearch.template.name}")
    private String templateName;

    @Value("${elasticsearch.template.version}")
    private Integer version;

    public void persist(ToDoListDto model) throws IOException {
        // TODO: bulk  ??
        IndexRequest request = new IndexRequest(journalIndexName, journalTypeName, model.getId().toString());
        request.opType(DocWriteRequest.OpType.INDEX);

        request.source(JsonOperationUtil.toJSON(model), XContentType.JSON);
        client.index(request, RequestOptions.DEFAULT);
    }

    public void createIndex() throws IOException {
        PutIndexTemplateRequest putRequest = new PutIndexTemplateRequest();
        putRequest.name(templateName)
                .patterns(Collections.singletonList(journalIndexName))
                .mapping(journalTypeName, elasticSearchSettings.getMappings(), XContentType.JSON)
                .settings(elasticSearchSettings.getSettings(), XContentType.JSON)
                .version(version);

        if (isLocalVersionGreater()) {
            client.indices().putTemplate(putRequest, RequestOptions.DEFAULT);
        }

    }

    public ResponseSearchDto filter(SearchSourceBuilder searchSourceBuilder) throws IOException {

        final SearchRequest searchRequest = new SearchRequest(journalIndexName);
        searchRequest.source(searchSourceBuilder);

        searchRequest.searchType(SearchType.DFS_QUERY_THEN_FETCH);

        final SearchResponse search = client.search(searchRequest, RequestOptions.DEFAULT);

        Collection<ToDoListDto> esResponse = Arrays.stream(search.getHits().getHits())
                .map(elem -> JsonOperationUtil.readFromJson(elem.getSourceAsString(), ToDoListDto.class))
                .collect(Collectors.toList());

        ResponseSearchDto response = new ResponseSearchDto();
        response.setItems(esResponse);
        response.setFoundCount(esResponse.size());
        response.setMaxAvailableCount((int) search.getHits().totalHits);

        return response;
    }

    private boolean isLocalVersionGreater() throws IOException {
        try {
            GetIndexTemplatesRequest getRequest = new GetIndexTemplatesRequest(templateName);
            GetIndexTemplatesResponse response = client.indices().getTemplate(getRequest, RequestOptions.DEFAULT);

            return version > response.getIndexTemplates().get(0).getVersion();
        } catch (Exception e) {
            return true;
        }
    }
}
