package com.potapov.todolist.dao;

import com.potapov.todolist.dto.todolist.ToDoListDto;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface ToDoListRepository extends CrudRepository<ToDoListDto, UUID> {


}
