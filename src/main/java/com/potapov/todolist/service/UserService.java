package com.potapov.todolist.service;

import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class UserService {

    public UUID findUserId() {
        return UUID.fromString("8bb262ff-5d12-44b1-810b-a7058762239b");
    }

}
