package com.potapov.todolist.service;

import com.potapov.todolist.dao.EsReposotory;
import com.potapov.todolist.dto.ResponseSearchDto;
import com.potapov.todolist.dto.todolist.ToDoListDto;
import com.potapov.todolist.helper.QueryHelper;
import com.potapov.todolist.model.FilterRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.concurrent.ThreadPoolExecutor;

@Slf4j
@Service
@RequiredArgsConstructor
public class FullTextSearchService {

    private final EsTaskQueueService esTaskQueueService;

    private final EsReposotory esReposotory;

    private final ThreadPoolExecutor syncPool;

    @PostConstruct
    private void postConstruct() throws Exception {
        esReposotory.createIndex();
    }

    public void addToQueueAndPersistAsync(ToDoListDto dto) {
        esTaskQueueService.addTask(dto.getId());
        syncPool.execute(() -> persist(dto));
    }

    public void persist(ToDoListDto dto) {
        try {
            // TODO: 06/10/2019 при апсерте теряем idшники вложенных струтур хибер их почему то не возвращает ((
            esReposotory.persist(dto);
            esTaskQueueService.removeTask(dto.getId());
        } catch (Exception e) {
            log.error("cant persist ");
        }

    }

    public ResponseSearchDto filter(FilterRequest request) {
        try {
            //get query
            SearchSourceBuilder searchSourceBuilder = QueryHelper.buildSearchRequest(request);

            //filter
            return esReposotory.filter(searchSourceBuilder);

        } catch (Exception ex) {
            log.error("cannot get journal by request {} from elastic Reason [{}]: {}", request, ex.getClass(), ex.getMessage());
            throw new RuntimeException("Can't get information from es");
        }
    }
}
