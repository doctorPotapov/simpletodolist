package com.potapov.todolist.service;

import com.google.common.collect.Lists;
import com.potapov.todolist.dao.EsTaskRepository;
import com.potapov.todolist.model.EsTask;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class EsTaskQueueService {

    private final EsTaskRepository esTaskRepository;

    @Transactional
    public void addTask(UUID listId) {
        esTaskRepository.save(new EsTask(listId));

    }

    @Transactional
    public void removeTask(UUID listId) {
        esTaskRepository.delete(new EsTask(listId));
    }

    public List<EsTask> findAllTask() {
        return Lists.newArrayList(esTaskRepository.findAll());
    }
}
