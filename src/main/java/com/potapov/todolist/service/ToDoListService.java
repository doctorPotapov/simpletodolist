package com.potapov.todolist.service;

import com.potapov.todolist.dao.ToDoListRepository;
import com.potapov.todolist.dto.ResponseSearchDto;
import com.potapov.todolist.dto.todolist.ToDoListDto;
import com.potapov.todolist.helper.FilterHelper;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@RequiredArgsConstructor
public class ToDoListService {

    private final FullTextSearchService fullTextSearchService;

    private final ToDoListRepository toDoListRepository;

    @Transactional
    public UUID persist(ToDoListDto dto) {
        toDoListRepository.save(dto);
        fullTextSearchService.addToQueueAndPersistAsync(dto);

        return dto.getId();
    }

    public ToDoListDto getOne(UUID id) {
        return toDoListRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Cant find list"));
    }

    public List<String> getAllNamesByUser(UUID userId) {
        final ResponseSearchDto allFields = fullTextSearchService.filter(FilterHelper.prepareFullTextSearchRequestByUsers(StringUtils.EMPTY, true, userId));
        return allFields.getItems().stream()
                .map(ToDoListDto::getListName)
                .collect(Collectors.toList());
    }

}
