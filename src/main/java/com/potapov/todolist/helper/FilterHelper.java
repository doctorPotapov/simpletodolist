package com.potapov.todolist.helper;

import com.potapov.todolist.model.FilterField;
import com.potapov.todolist.model.FilterRequest;
import com.potapov.todolist.model.enums.Filters;
import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

@UtilityClass
public class FilterHelper {

    private static final int DEFAULT_LIMIT = 200;

    private static final int DEFAULT_OFFSET = 0;

    public FilterRequest prepareFullTextSearchRequestByUsers(String searchedText, boolean showArchive, UUID userId) {
        FilterRequest request = new FilterRequest();
        request.setLimit(DEFAULT_LIMIT);
        request.setOffset(DEFAULT_OFFSET);

        Collection<FilterField> fields = new ArrayList<>();

        if (showArchive) {
            fields.add(new FilterField(Filters.IS_ARCHIVE, "true"));
        }

        Optional.ofNullable(searchedText)
                .filter(StringUtils::isNotBlank)
                .ifPresent(res -> fields.add(new FilterField(Filters.TEXT, res)));

        fields.add(new FilterField(Filters.USER, userId.toString()));

        request.setFields(fields);

        return request;
    }

}
