package com.potapov.todolist.helper;

import com.potapov.todolist.model.FilterRequest;
import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.BooleanUtils;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@UtilityClass
public class QueryHelper {
    public static SearchSourceBuilder buildSearchRequest(FilterRequest request) {

        Collection<QueryBuilder> elasticFilters = collectQueries(request);

        return createRequestByFields(request.getOffset(), request.getLimit(), elasticFilters);
    }

    private static Collection<QueryBuilder> collectQueries(FilterRequest dto) {
        Collection<QueryBuilder> elasticFilters = new ArrayList<>();

        dto.getFields().forEach(elem -> {
            switch (elem.getField()) {
                case TEXT:
                    final List<MatchQueryBuilder> matchQueryBuilders = Arrays.asList(
                            QueryBuilders.matchQuery("listName", elem.getValue()).operator(Operator.AND).boost(20),
                            QueryBuilders.matchQuery("elem.description", elem.getValue()).operator(Operator.AND).boost(10)
                    );
                    final BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
                    boolQueryBuilder.should().addAll(matchQueryBuilders);
                    elasticFilters.add(boolQueryBuilder);
                    break;
                case USER:
                    elasticFilters.add(QueryBuilders.termQuery("userId", elem.getValue()));
                    break;
                case IS_ARCHIVE:
                    if (BooleanUtils.toBoolean(elem.getValue())) {
                        elasticFilters.add(QueryBuilders.boolQuery().mustNot(QueryBuilders.termQuery("archive", true)));
                    }
                    break;
            }
        });

        return elasticFilters;
    }

    private static SearchSourceBuilder createRequestByFields(int from, int size, Collection<QueryBuilder> filters) {
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        BoolQueryBuilder fullQuery = QueryBuilders.boolQuery();


        // TODO: 06/10/2019 sorts?
//        if (sorts != null) {
//            searchSourceBuilder.sort(sorts);
//        }

        fullQuery.must().addAll(filters);

        searchSourceBuilder.query(fullQuery);
        searchSourceBuilder.from(from);
        searchSourceBuilder.size(size);

        return searchSourceBuilder;
    }
}
