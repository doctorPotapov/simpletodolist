package com.potapov.todolist.controller;

import com.potapov.todolist.dto.ResponseDto;
import com.potapov.todolist.dto.ResponseSearchDto;
import com.potapov.todolist.dto.todolist.ToDoListDto;
import com.potapov.todolist.helper.FilterHelper;
import com.potapov.todolist.model.FilterRequest;
import com.potapov.todolist.service.FullTextSearchService;
import com.potapov.todolist.service.ToDoListService;
import com.potapov.todolist.service.UserService;
import com.potapov.todolist.validator.RequestValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;


@RestController
@RequestMapping("api/")
@ResponseBody
@RequiredArgsConstructor
public class ToDoController {

    private final RequestValidator validator;

    private final ToDoListService toDoListService;

    private final FullTextSearchService fullTextSearchService;

    private final UserService userService;

    @PostMapping(value = "create")
    public ResponseDto createList(@RequestBody ToDoListDto dto) {
        validator.validate(dto);
        UUID userId = userService.findUserId();
        dto.setUserId(userId);
        UUID id = toDoListService.persist(dto);
        ResponseDto response = new ResponseDto();
        response.setEntityID(id);

        return response;
    }

    @GetMapping(value = "getAllById")
    public ResponseDto getListById(@RequestParam UUID id) {
        ToDoListDto one = toDoListService.getOne(id);
        ResponseDto response = new ResponseDto();
        response.setEntityID(one.getId());
        response.setToDoListDto(one);

        return response;
    }

    @GetMapping(value = "filter")
    public ResponseSearchDto filter(@RequestParam String text) {
        validator.validate(text);
        FilterRequest request = FilterHelper.prepareFullTextSearchRequestByUsers(text, false, userService.findUserId());
        return fullTextSearchService.filter(request);
    }

    @GetMapping(value = "getAllLists")
    public List<String> getAllLists() {
        return toDoListService.getAllNamesByUser(userService.findUserId());
    }
}
