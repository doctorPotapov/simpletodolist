package com.potapov.todolist.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.potapov.todolist.dto.todolist.ToDoListDto;
import lombok.Data;

import java.util.UUID;

@Data
@JsonSerialize
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseDto {
    private UUID entityID;
    private String msg;
    private ToDoListDto toDoListDto;
}
