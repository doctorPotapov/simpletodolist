package com.potapov.todolist.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.potapov.todolist.dto.todolist.ToDoListDto;
import lombok.Data;

import java.util.Collection;

@Data
@JsonSerialize
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseSearchDto {
    public Collection<ToDoListDto> items;
    public Integer foundCount;
    public Integer maxAvailableCount;
}
