package com.potapov.todolist.dto.todolist;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "ListsElem")
@Table(name = "lists_elem")
@Data
public class ListElem {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "id", strategy = "uuid2")
    @Column(name = "id", unique = true)
    private UUID id;

    private boolean done;

    private String description;

}
