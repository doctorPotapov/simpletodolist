package com.potapov.todolist.dto.todolist;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@JsonSerialize
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Entity(name = "Lists")
@Table(name = "lists")
public class ToDoListDto {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "id", strategy = "uuid2")
    @Column(name = "id", unique = true)
    private UUID id;

    @Column(name = "name")
    private String listName;

    @Column(name = "is_archive")
    private boolean isArchive;

    private UUID userId;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "lists_id")
    private List<ListElem> elem;


}
