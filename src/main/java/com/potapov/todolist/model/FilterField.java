package com.potapov.todolist.model;

import com.potapov.todolist.model.enums.Filters;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FilterField {

    public Filters field;
    public String value;

}
