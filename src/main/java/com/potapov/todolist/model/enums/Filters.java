package com.potapov.todolist.model.enums;

public enum  Filters {
    USER,
    TEXT,
    IS_ARCHIVE

}
