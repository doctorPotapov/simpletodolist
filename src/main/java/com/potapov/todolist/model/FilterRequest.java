package com.potapov.todolist.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Collection;

@NoArgsConstructor
@Getter
@Setter
public class FilterRequest {
    public static final String SORT_ASC = "asc";
    public static final String SORT_DESC = "desc";
    private Integer offset;
    private Integer limit;
    private Collection<FilterField> fields;
    private Collection<FilterField> sort;

}
