package com.potapov.todolist.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity(name = "EsTask")
@Table(name = "esQueue")
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class EsTask {

    @Id
    private UUID entityId;
}
