package com.potapov.todolist.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

@Slf4j
@UtilityClass
public class JsonOperationUtil {

    // TODO: 06/10/2019 is it safe?
    private static final ObjectMapper jsonMapper = new ObjectMapper();

    public static String toJSON(Object object) {
        if (object == null) {
            return null;
        } else {
            try {
                return jsonMapper.writeValueAsString(object);
            } catch (IOException var2) {
                log.error("Cannot serialize object {} to json. Reason [{}] : {}", object, var2.getClass(), var2.getMessage());
                return null;
            }
        }
    }

    public static <T> T readFromJson(String json, Class<T> clazz) {
        try {
            return jsonMapper.readValue(json, clazz);
        } catch (IOException e) {
            return null;
        }

    }

}
