package com.potapov.todolist.scheduller;

import com.potapov.todolist.dto.todolist.ToDoListDto;
import com.potapov.todolist.model.EsTask;
import com.potapov.todolist.service.EsTaskQueueService;
import com.potapov.todolist.service.FullTextSearchService;
import com.potapov.todolist.service.ToDoListService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * переиндексация недолетевших событий
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class EsBackup {

    private final EsTaskQueueService esTaskQueueService;

    private final FullTextSearchService fullTextSearchService;

    private final ToDoListService toDoListService;

    @Value("${system.replication:replica}")
    private String replication;

    @Scheduled(cron = "0/10 * * * * ?")
    public void checkAndRecallEvents() {
        if (replication.equals("master")) {

            List<EsTask> tasks = esTaskQueueService.findAllTask();

            if (!tasks.isEmpty()) {
                log.debug("get {} tasks to synchronize", tasks.size());
                tasks.forEach(this::process);
                log.debug("synchronize for {} tasks complete", tasks.size());
            }
        }
    }

    private void process(EsTask task){
        final ToDoListDto list = toDoListService.getOne(task.getEntityId());
        fullTextSearchService.persist(list);
        esTaskQueueService.removeTask(task.getEntityId());
    }
}
