package com.potapov.todolist.configurations;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

@Configuration
public class Pools {

    @Value("${threadPools.syncPoolSize:10}")
    private Integer syncPoolSize;

    @Bean
    public ThreadPoolExecutor syncPool() {
        return (ThreadPoolExecutor) Executors.newFixedThreadPool(syncPoolSize);
    }
}
