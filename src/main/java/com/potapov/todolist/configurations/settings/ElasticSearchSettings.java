package com.potapov.todolist.configurations.settings;

import lombok.Getter;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

/**
 * Настройки индекса для ES
 */
@Getter
@Component
public class ElasticSearchSettings {

    private final String mappings;

    private final String settings;

    public ElasticSearchSettings() throws IOException {

        ClassLoader loader = getClass().getClassLoader();
        URL indexMappingsUrl = loader.getResource("elastic/todolist-index-mappings.json");
        URL indexSettingsUrl = loader.getResource("elastic/todolist-index-settings.json");

        settings = IOUtils.toString(Objects.requireNonNull(indexSettingsUrl), StandardCharsets.UTF_8);
        mappings = IOUtils.toString(Objects.requireNonNull(indexMappingsUrl), StandardCharsets.UTF_8);
    }


}
