package com.potapov.todolist.configurations;

import org.apache.http.HttpHost;
import org.apache.http.impl.nio.reactor.IOReactorConfig;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.elasticsearch.rest.RestClientProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Настройки ES
 */
@Configuration
@EnableConfigurationProperties(RestClientProperties.class)
public class EsConfiguration {

    @Value("${elasticsearch.host}")
    private String host;

    @Value("${elasticsearch.port}")
    private int port;

    @Value("${elasticsearch.client.connectionTimeOutMillis}")
    private int connectionTimeOut;
    @Value("${elasticsearch.client.socketTimeOutMillis}")
    private int socketTimeOut;
    @Value("${elasticsearch.client.maxRetryTimeOuMillis}")
    private int maxRetryTimeOutMilis;
    @Value("${elasticsearch.client.restClientThreadCount}")
    private int restClientThreadCount;
    @Value("${elasticsearch.client.connectionRequestTimeout}")
    private int connectionRequestTimeout;

    @Bean
    public RestClientBuilder taskRestClientBuilder() {
        return RestClient.builder(new HttpHost(host, port, HttpHost.DEFAULT_SCHEME_NAME))
                .setRequestConfigCallback(builder ->
                        builder.setConnectTimeout(connectionTimeOut)
                                .setSocketTimeout(socketTimeOut)
                                .setConnectionRequestTimeout(connectionRequestTimeout))
                .setMaxRetryTimeoutMillis(maxRetryTimeOutMilis)
                .setHttpClientConfigCallback(httpClientBuilder -> httpClientBuilder.setDefaultIOReactorConfig(
                        IOReactorConfig.custom().setIoThreadCount(restClientThreadCount).build()));
    }

    @Bean
    public RestHighLevelClient restClientBuilder(RestClientBuilder taskRestClientBuilder) {
        return new RestHighLevelClient(taskRestClientBuilder);
    }

}
