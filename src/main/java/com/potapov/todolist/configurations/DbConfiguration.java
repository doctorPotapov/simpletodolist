package com.potapov.todolist.configurations;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.configuration.FluentConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class DbConfiguration {

    @Bean(initMethod = "migrate")
    public Flyway flyway(DataSource dataSource) {
        FluentConfiguration configure = Flyway.configure();
        configure.locations("classpath:db/migration/");
        configure.dataSource(dataSource);
        configure.baselineOnMigrate(true);

        return configure.load();
    }

}
