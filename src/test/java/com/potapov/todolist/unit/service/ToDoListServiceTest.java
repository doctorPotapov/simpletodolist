package com.potapov.todolist.unit.service;

import com.potapov.todolist.dao.ToDoListRepository;
import com.potapov.todolist.dto.todolist.ToDoListDto;
import com.potapov.todolist.service.FullTextSearchService;
import com.potapov.todolist.service.ToDoListService;
import com.potapov.todolist.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ToDoListServiceTest {

    @InjectMocks
    private ToDoListService toDoListService;

    @Mock
    private FullTextSearchService fullTextSearchService;

    @Mock
    private ToDoListRepository toDoListRepository;

    @Test
    void persistCheck() {
        ToDoListDto dto = new ToDoListDto();

        toDoListService.persist(dto);
    }

    @Test
    void getOneCheck() {
        UUID existedEntityId = UUID.randomUUID();

        when(toDoListRepository.findById(existedEntityId))
                .thenReturn(Optional.of(new ToDoListDto()));

        ToDoListDto actuals = toDoListService.getOne(existedEntityId);

        assertNotNull(actuals);
    }

    @Test
    void getOneFailedCheck() {
        UUID existedEntityId = UUID.randomUUID();
        UUID notExistedEntityId = UUID.randomUUID();

        when(toDoListRepository.findById(existedEntityId))
                .thenReturn(Optional.of(new ToDoListDto()));

        ToDoListDto actuals = toDoListService.getOne(existedEntityId);

        assertNotNull(actuals);

        try {
            toDoListService.getOne(notExistedEntityId);
            fail();
        } catch (RuntimeException e) {
            assertEquals(e.getMessage(), "Cant find list");
        }
    }
}