package com.potapov.todolist.service;

import com.potapov.todolist.configuration.SpringTestConfiguration;
import com.potapov.todolist.dao.EsTaskRepository;
import com.potapov.todolist.model.EsTask;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;


@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {SpringTestConfiguration.class})
class EsTaskQueueServiceTest {

    @Autowired
    private EsTaskQueueService esTaskQueueService;

    @Autowired
    private EsTaskRepository esTaskRepository;

    @Test
    void addTask() {
        final UUID testEntity = UUID.randomUUID();

        esTaskQueueService.addTask(testEntity);
        final EsTask task = esTaskRepository.findById(testEntity)
                .orElse(null);

        assertNotNull(task);
    }

    @Test
    void removeTask() {
        final UUID testEntity = UUID.randomUUID();

        esTaskQueueService.addTask(testEntity);
        EsTask task = esTaskRepository.findById(testEntity)
                .orElse(null);

        assertNotNull(task);

        esTaskQueueService.removeTask(testEntity);

        task = esTaskRepository.findById(testEntity)
                .orElse(null);

        assertNull(task);


    }

    @AfterEach
    void clearAll() {
        esTaskRepository.deleteAll();
    }
}